#include "Vector.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

void main() {
	Vector vector = Vector(2);

	vector.push_back(1);
	vector.push_back(2);
	vector.push_back(3);
	cout << "Pushed in 1, 2, 3" << endl << vector << endl;

	int poppedOut = vector.pop_back();
	cout << endl << "Popped Out: " << poppedOut << endl << vector << endl;

	vector.reserve(5);
	cout << endl << "Reserve 5" << endl << vector << endl;

	vector.push_back(4);
	vector.push_back(5);
	vector.push_back(6);
	cout << endl << "Pushed in 4, 5, 6" << endl << vector << endl;

	vector.resize(2);
	cout << endl << "Resized to 2" << endl << vector << endl;

	vector.assign(70);
	cout << endl << "Assign 70" << endl << vector << endl;

	vector.resize(8, 60);
	cout << endl << "Resize 8, 60" << endl << vector << endl;

	vector.resize(5, 50);
	cout << endl << "Resize 5, 60" << endl << vector << endl;

	Vector vector2 = vector;
	cout << endl << "Vector2" << endl << vector2 << endl;

	cout << endl << "Index 3 number: " << vector[3] << endl;

	vector[3] = 8;
	cout << endl << "Index 3 changed to 8" << endl << vector << endl;

	Vector* vector3 = vector + vector2;
	cout << endl << "Sum of vector1 and vector2" << endl << *vector3 << endl;

	Vector* vector4 = vector - vector2;
	cout << endl << "Mana of vector1 and vector2" << endl << *vector4 << endl;

	vector += vector2;
	cout << endl << "Sum of vector1 and vector2 using +=" << endl << vector << endl;

	vector -= vector2;
	cout << endl << "Mana of vector1 and vector2 using -=" << endl << vector << endl;

	delete vector3;
	delete vector4;

	system("pause");
}