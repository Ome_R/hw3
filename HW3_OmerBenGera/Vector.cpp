#include "Vector.h"
#include <string>

Vector::Vector(int n)
{
	if (n < 2)
		n = 2;

	_elements = new int[n];
	_capacity = _resizeFactor = n;
	_size = 0;
}

Vector::~Vector()
{
	delete[] _elements;
}

int Vector::size() const
{
	return _size;
}

int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty() const
{
	return _size == 0;
}

void Vector::push_back(const int & val)
{
	if (_size < _capacity) {
		_elements[_size] = val;
		_size++;
	}
	else {
		//Relocate memory for _elements.
		reserve(_capacity + 1);
		//We can recall push_back
		push_back(val);
	}
}

int Vector::pop_back()
{
	if (empty()) {
		std::cout << "Error: pop from empty vector." << std::endl;
		return -9999;
	}

	_size -= 1;

	return _elements[_size];
}

void Vector::reserve(int n)
{
	if (_capacity < n) {
		//Create a new elements array
		int* newElements = new int[_capacity + _resizeFactor];

		_capacity += _resizeFactor;

		//Copy all vars to the new array
		for (int i = 0; i < _size; i++) {
			newElements[i] = _elements[i];
		}

		//Replace _elements with newElements
		delete[] _elements;
		_elements = newElements;

		//We can call again the reserve method.
		// The recursive loop will be stopped when _capacity equals or higher than n.
		reserve(n);
	}
}

void Vector::resize(int n)
{
	if (n > _capacity) {
		reserve(n);
	}

	_size = n;

}

void Vector::assign(int val)
{
	for (int i = 0; i < _size; i++) {
		_elements[i] = val;
	}
}

void Vector::resize(int n, const int & val)
{
	resize(n);
	assign(val);
}

Vector::Vector(const Vector & other)
{
	this->_size = other._size;
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;

	//Deep copy to elements
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++) {
		this->_elements[i] = other._elements[i];
	}
}

Vector & Vector::operator=(const Vector & other)
{
	Vector* vector = new Vector(other);
	return *vector;
}

int & Vector::operator[](int n) const
{
	if (n >= _size) {
		std::cout << "Error: index " << n << " is out of array bounds." << std::endl;
		n = 0;
	}
	
	return _elements[n];
}

Vector* Vector::operator+(const Vector & other) const
{
	int size = _size < other._size ? _size : other._size;
	Vector* vector = new Vector(size);

	for (int i = 0; i < size; i++) {
		vector->push_back(this->_elements[i] + other._elements[i]);
	}

	return vector;
}

Vector* Vector::operator-(const Vector & other) const
{
	int size = _size < other._size ? _size : other._size;
	Vector* vector = new Vector(size);

	for (int i = 0; i < size; i++) {
		vector->push_back(this->_elements[i] - other._elements[i]);
	}

	return vector;
}

Vector& Vector::operator+=(const Vector & other)
{
	int size = _size < other._size ? _size : other._size;
	for (int i = 0; i < size; i++) {
		_elements[i] += other._elements[i];
	}
	return *this;
}

Vector & Vector::operator-=(const Vector & other)
{
	int size = _size < other._size ? _size : other._size;
	for (int i = 0; i < size; i++) {
		_elements[i] -= other._elements[i];
	}
	return *this;
}

std::ostream & operator<<(std::ostream & os, const Vector & vector)
{
	std::string data = "";

	for (int i = 0; i < vector._size; i++) {
		data += ", ";
		data += vector[i];
	}

	std::cout << "Vector Info" << std::endl 
		<< "Capacity is " << vector.capacity() << std::endl 
		<< "Size is " << vector.size() << std::endl
		<< "Data is {";

	for (int i = 0; i < vector._size; i++) {
		if (i != 0)
			std::cout << ", ";
		std::cout << vector[i];
	}

	std::cout << "}" << std::endl;

	return os;
}
